# URL Minify

A URL minifier/shortener intended for local use.

## Install

* Install a local web server, like apache or nginx.
  E.g. On a Debian system, `apt install apache2`
* Add a new u.rl entry in /etc/hosts.
  E.g. `127.0.0.1    u.rl`
* Create a virtualhost pointing docroot to `$DATA_DIR`.
  ```
  # apache
  <VirtualHost 127.0.0.1:80>
    ServerName u.rl
    DocumentRoot /path/to/home/.urlminify
    <Directory /path/to/home/.urlminify>
      Require all granted
    </Directory>
  </VirtualHost>
  ```
